//
//  WebViewViewController.swift
//  Mandiri_Test_Project
//
//  Created by Nanda Mochammad on 27/06/21.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    var url = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(url)
        if !isConnectedToNetwork() {
            self.presentAlertPerhatian(message: "Koneksi internet anda tidak tersedia, harap menggunakan koneksi yang stabil")
        } else {
            webView.load(NSURLRequest(url: NSURL(string: url)! as URL) as URLRequest)
        }
    }
}
