//
//  ViewController.swift
//  Mandiri_Test_Project
//
//  Created by Nanda Mochammad on 27/06/21.
//

import UIKit

enum PageType {
    case category, source, article
}

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    lazy var searchBar:UISearchBar = UISearchBar()

    var viewModel = ControllerViewModel()
    var pagetType = PageType.source
    var isProgressLoad  = false
    var textSearch = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        viewModel = GlobalData.shared.viewModel ?? ControllerViewModel()
        viewModel.removeArticleData()
        DispatchQueue.main.async {
            self.getData()
        }
    }


}

//MARK:- View
extension ViewController {
    func setupView() {
        if pagetType == .article {
            searchBar.searchBarStyle = UISearchBar.Style.default
            searchBar.sizeToFit()
            searchBar.isTranslucent = false
            searchBar.delegate = self
            navigationItem.titleView = searchBar
        }
    }
    
    func getData() {
        if !isConnectedToNetwork() {
            self.presentAlertPerhatian(message: "Koneksi internet anda tidak tersedia, harap menggunakan koneksi yang stabil")
        } else {
            if pagetType == .source {
                viewModel.getSource(category: viewModel.selectedCategory) { response, err in
                    self.tableView.reloadData()
                }
            } else if pagetType == .article {
                let domain = viewModel.getDomainFromUrl(url: viewModel.selectedSource?.url ?? "")
                let page = viewModel.pageToLoad()
                if !isProgressLoad && page != 0 {
                        isProgressLoad = true
                        viewModel.getArticle(category: viewModel.selectedCategory, domain: domain, page: page) { response, err in
                            self.tableView.reloadData()
                            self.isProgressLoad = false
                            self.view.endEditing(true)
                            self.searchBar.endEditing(true)
                        }
                    
                }
            }
        }
    }
    
    func searchArticle(title: String) {
        if !isConnectedToNetwork() {
            self.presentAlertPerhatian(message: "Koneksi internet anda tidak tersedia, harap menggunakan koneksi yang stabil")
        } else {
            let domain = viewModel.getDomainFromUrl(url: viewModel.selectedSource?.url ?? "")
            let page = viewModel.pageToLoad()
            if page != 0 && !isProgressLoad{
                isProgressLoad = true
                viewModel.searchArticle(title: title, domain: domain, page: page) { response, err in
                    self.tableView.reloadData()
                    self.isProgressLoad = false
                }
            }
        }
    }
}

//MARK:- Delegate
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch pagetType {
        case .category:
            return Constant().categories.count
        case .source:
            return viewModel.dataSource.count
        default:
            return viewModel.dataArticle.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch pagetType {
        case .category:
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell") as! CategoryTableViewCell
            cell.setupDataCategory(title: Constant().categories[indexPath.row].capitalized)
            return cell
        case .source:
            let cell = tableView.dequeueReusableCell(withIdentifier: "sourceCell") as! CategoryTableViewCell
            cell.setupDataSource(category: viewModel.selectedCategory, data: viewModel.dataSource[indexPath.row])
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "articleCell") as! CategoryTableViewCell
            cell.setupArticle(data: viewModel.dataArticle[indexPath.row])
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch pagetType {
        case .category:
            return "Silakan pilih kategori berita:"
        case .source:
            return "Silakan pilih sumber berita:"
        default:
            return "Silakan pilih berita:"
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        GlobalData.shared.viewModel = self.viewModel

        switch pagetType {
        case .category:
            viewModel.selectedCategory = Constant().categories[indexPath.row]
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            vc.pagetType = .source
            vc.title = viewModel.selectedCategory.capitalized
            self.navigationController?.pushViewController(vc, animated: true)
        case .source:
            viewModel.selectedSource = viewModel.dataSource[indexPath.row]
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            vc.pagetType = .article
            vc.title = viewModel.selectedSource?.name?.capitalized
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            viewModel.selectedArticle = viewModel.dataArticle[indexPath.row]
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            vc.url = (viewModel.selectedArticle?.url)!
            vc.title = viewModel.selectedArticle?.title?.capitalized
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height )){
            getData()
        }
   }
}

//MARK:-SearchBar
extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == ""{
            searchBar.resignFirstResponder()
            textSearch = ""
            self.viewModel.removeArticleData()
            getData()
            self.searchBar.endEditing(true)
            self.view.endEditing(true)
        }else{
            textSearch = searchText
            searchArticle(title: textSearch)
            DispatchQueue.main.async {
                self.searchBar.endEditing(true)
            }
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        DispatchQueue.main.async {
            searchBar.resignFirstResponder()
            self.viewModel.removeArticleData()
            self.getData()
            self.view.endEditing(true)
            self.searchBar.endEditing(true)
        }
    }
    
    
}
