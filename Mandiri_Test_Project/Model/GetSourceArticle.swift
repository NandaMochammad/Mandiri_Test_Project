//
//  GetSourceArticle.swift
//  Mandiri_Test_Project
//
//  Created by Nanda Mochammad on 27/06/21.
//

import Foundation

struct GetSourceArticleResponse: Codable {
    var status: String
    var sources: [DataSource]
}

struct GetSourceArticleRequest {
    private static func resourceRequest(cat: String)->WebserviceResource<GetSourceArticleResponse>{
        let url = Constant().sourcesURL(category: cat)
    
        return WebserviceResource<GetSourceArticleResponse>(url: URL(string: url.replaceSpaceInURL())!)
    }
    
    static func requestSource(cat: String, completion: @escaping(Bool, GetSourceArticleResponse?, Error?)->Void){
        Indicator.sharedInstance.showIndicator()
        Webservice().requestGET(resource: GetSourceArticleRequest.resourceRequest(cat: cat)) { (response) in
            Indicator.sharedInstance.hideIndicator()
            switch response {
            case .success(let result):
                completion(true, result, nil)
            case .failure(let err):
                completion(false, nil, err)
            }
        }
    }
}
