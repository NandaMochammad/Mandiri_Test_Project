//
//  GetGenreModel.swift
//  Mandiri_Test_Project
//
//  Created by Nanda Mochammad on 27/06/21.
//

import Foundation

struct GetSearchArticleResponse: Codable {
    var status: String
    var totalResults: Int
    var articles: [DataArticle]
}

struct DataArticle: Codable {
    var title: String?
    var description: String?
    var url: String?
    var urlToImage: String?
}

struct DataSource: Codable {
    var id: String?
    var name: String?
    var description: String?
    var url: String?
    var category: String?
    var language: String?
    var country: String?
}

struct GetSearchArticleRequest {
    private static func resourceRequest(title: String, domain: String, page: Int)->WebserviceResource<GetSearchArticleResponse>{
        let url = Constant().searchNewsURL(title: title, domain: domain, page: page)
    
        return WebserviceResource<GetSearchArticleResponse>(url: URL(string: url.replaceSpaceInURL())!)
    }
    
    static func requestSearch (title: String, domain: String, page:Int, completion: @escaping(GetSearchArticleResponse?, Error?)->Void){
        Indicator.sharedInstance.showIndicator()
        Webservice().requestGET(resource: GetSearchArticleRequest.resourceRequest(title: title, domain: domain, page: page)) { (response) in
            Indicator.sharedInstance.hideIndicator()
            switch response {
            case .success(let result):
                completion(result, nil)
            case .failure(let err):
                completion(nil, err)
            }
        }
    }
}
