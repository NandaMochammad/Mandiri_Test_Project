//
//  GetArticle.swift
//  Mandiri_Test_Project
//
//  Created by Nanda Mochammad on 27/06/21.
//

import Foundation

struct GetArticleResponse: Codable {
    var status: String
    var totalResults: Int
    var articles: [DataArticle]
}

struct GetArticleRequest {
    static func resourceRequest(cat: String, domain: String, page: Int)->WebserviceResource<GetArticleResponse>{
        let url = Constant().articleURL(category: cat, domain: domain, page: page)
    
        return WebserviceResource<GetArticleResponse>(url: URL(string: url.replaceSpaceInURL())!)
    }
    
    static func requestSource(cat: String, domain: String, page: Int, completion: @escaping(Bool, GetArticleResponse?, Error?)->Void){
        Indicator.sharedInstance.showIndicator()
        Webservice().requestGET(resource: GetArticleRequest.resourceRequest(cat: cat, domain: domain, page: page)) { (response) in
            Indicator.sharedInstance.hideIndicator()
            switch response {
            case .success(let result):
                completion(true, result, nil)
            case .failure(let err):
                completion(false, nil, err)
            }
        }
    }
}
