//
//  SourceTableViewCell.swift
//  Mandiri_Test_Project
//
//  Created by Nanda Mochammad on 27/06/21.
//

import UIKit
import SDWebImage

class CategoryTableViewCell: UITableViewCell {
    @IBOutlet weak var titleCategory: UILabel!
    
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var imageArticle: UIImageView!
    @IBOutlet weak var titleArticle: UILabel!
    @IBOutlet weak var descriptionArticle: UILabel!
    
    
    func setupDataCategory(title: String) {
        titleCategory.text = title
    }
    
    func setupDataSource(category: String, data: DataSource) {
        sourceLabel.text = data.name
        descriptionLabel.text = data.description
    }
    
    func setupArticle(data: DataArticle) {
        imageArticle.image = UIImage(named: "imgDefault")
        
        titleArticle.text = data.title
        descriptionArticle.text = data.description
        
//        if let img = data.urlToImage {
//            if let url = URL(string: img) {
//            }
//        }
        
        imageArticle.layer.masksToBounds = false
        imageArticle.layer.shadowColor = UIColor.black.cgColor
        imageArticle.layer.shadowOpacity = 0.3
        imageArticle.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        imageArticle.layer.shadowRadius = 15
        imageArticle.layer.cornerRadius = 15
        imageArticle.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        imageArticle.layer.shouldRasterize = true
        imageArticle.layer.rasterizationScale = UIScreen.main.scale
        
        guard let img = data.urlToImage, let url = URL(string: img) else { return }
        imageArticle.sd_setImage(with: url, completed: nil)
    }
}
