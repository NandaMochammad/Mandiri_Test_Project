//
//  SourceViewModel.swift
//  Mandiri_Test_Project
//
//  Created by Nanda Mochammad on 27/06/21.
//

import Foundation

class ControllerViewModel: NSObject {
    
    var dataSource: [DataSource] = []
    var dataArticle: [DataArticle] = [] {
        didSet{
            currentArticleTotal = dataArticle.count
        }
    }
    var selectedSource: DataSource?
    var selectedArticle: DataArticle?
    var selectedCategory: String = ""
    
    var articlePage = 0
    var currentArticleTotal = 0
    var totalArticleAvailable = 0
    
    
    func removeArticleData() {
        articlePage = 0
        currentArticleTotal = 0
        totalArticleAvailable = 0
        dataArticle = []
    }
    
    func pageToLoad() -> Int {
        if currentArticleTotal != 0 {
            if currentArticleTotal < 20 {
                articlePage += 1
                return articlePage
            } else {
                return 0
            }
        } else if currentArticleTotal == 0{
            articlePage = 1
            return articlePage
        } else {
            return 0
        }
    }
    
    func getDomainFromUrl(url: String) -> String{
        let removeTag = ["https://", "http://", "https://www.", "http://www."]
        var resultDomain = ""
        
        removeTag.forEach { removeData in
            if url.contains(removeData) {
                resultDomain = url.replacingOccurrences(of: removeData, with: "")
            }
        }
        
        return resultDomain
    }
    
    func getSource(category: String, completion: @escaping(GetSourceArticleResponse?, Error?)->Void) {
        GetSourceArticleRequest.requestSource(cat: category) { isDone, res, err in
            if let error = err {
                completion(nil, error)
            } else  {
                self.dataSource = res!.sources
                completion(res, nil)
            }
        }
    }
    
    func getArticle(category: String, domain: String, page: Int, completion: @escaping(GetArticleResponse?, Error?)->Void) {
        GetArticleRequest.requestSource(cat: category, domain: domain, page: page) { isDone, res, err in
            if let error = err {
                completion(nil, error)
            } else  {
                self.dataArticle.append(contentsOf: res!.articles)
                self.totalArticleAvailable = res!.totalResults
                completion(res, nil)
            }
        }
    }
    
    func searchArticle(title: String, domain: String, page: Int, completion: @escaping(GetSearchArticleResponse?, Error?)->Void) {
        GetSearchArticleRequest.requestSearch(title: title, domain: domain, page: page) { res, err in
            if let error = err {
                completion(nil, error)
            } else  {
                self.dataArticle = res!.articles
                self.totalArticleAvailable = res!.totalResults
                completion(res, nil)
            }
        }
    }
    
}
