//
//  Constant.swift
//  Mandiri_Test_Project
//
//  Created by Nanda Mochammad on 27/06/21.
//

import Foundation

struct Constant {
    let api: String = "daf1a049bda94a8bb510f0f2f5708c51"
    
    let categories = ["business","entertainment","general","health","science","sports","technology"]
    
    //&country=id
    func searchNewsURL(title: String, domain: String, page: Int)-> String {
        return "https://newsapi.org/v2/everything?q=\(title.replaceSpaceInURL())&pageSize=5&page=\(page)&sortBy=popularity&apiKey=\(api)&language=en"
    }
    
    func sourcesURL(category: String)-> String{
        return "https://newsapi.org/v2/sources?apiKey=\(api)&language=en"
    }
    
    func articleURL(category: String, domain: String, page: Int)-> String {
        return "https://newsapi.org/v2/everything?domains=\(domain)&pageSize=10&page=\(page)&apiKey=\(api)&language=en"
    }
}
