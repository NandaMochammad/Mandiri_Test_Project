//
//  Ext+String.swift
//  Mandiri_Test_Project
//
//  Created by Nanda Mochammad on 27/06/21.
//

import UIKit
import Alamofire

enum HMACAlgorithm {
    case MD5, SHA1, SHA224, SHA256, SHA384, SHA512
    
    func toCCHmacAlgorithm() -> CCHmacAlgorithm {
        var result: Int = 0
        switch self {
        case .MD5:
            result = kCCHmacAlgMD5
        case .SHA1:
            result = kCCHmacAlgSHA1
        case .SHA224:
            result = kCCHmacAlgSHA224
        case .SHA256:
            result = kCCHmacAlgSHA256
        case .SHA384:
            result = kCCHmacAlgSHA384
        case .SHA512:
            result = kCCHmacAlgSHA512
        }
        return CCHmacAlgorithm(result)
    }
    
    func digestLength() -> Int {
        var result: CInt = 0
        switch self {
        case .MD5:
            result = CC_MD5_DIGEST_LENGTH
        case .SHA1:
            result = CC_SHA1_DIGEST_LENGTH
        case .SHA224:
            result = CC_SHA224_DIGEST_LENGTH
        case .SHA256:
            result = CC_SHA256_DIGEST_LENGTH
        case .SHA384:
            result = CC_SHA384_DIGEST_LENGTH
        case .SHA512:
            result = CC_SHA512_DIGEST_LENGTH
        }
        return Int(result)
    }
}

class RandomStringItem {
    static let characters = Array("12345789")
    static let length = UInt32(characters.count)
    
    private let encryptionKey = "bEDytW8VGZky1RPSNdZ5UyxxO+SLp1m4BnE9nPEASHQ="
    private let encryptionIv = "QeaRw4VG4RbY95oe+3/gEHb3SpsKPrLUh/5Ciik73zc="
    
    func getEncryptionKey() -> String {
        return encryptionKey.decrypt(key: "jKe3Ang4ShimWc9a", ivString: "pDjj8Scgh1hfgki4")
    }
    
    func getEncryptionIv() -> String {
        return encryptionIv.decrypt(key: "jKe3Ang4ShimWc9a", ivString: "pDjj8Scgh1hfgki4")
    }
}

extension String {
    func hmac(algorithm: HMACAlgorithm, key: String) -> String {
        if let cKey = key.cString(using: String.Encoding.utf8), let cData = self.cString(using: String.Encoding.utf8) {
            var result = [CUnsignedChar](repeating: 0, count: Int(algorithm.digestLength()))
            CCHmac(algorithm.toCCHmacAlgorithm(), cKey, strlen(cKey), cData, strlen(cData), &result)
            let hmacData: NSData = NSData(bytes: result, length: (Int(algorithm.digestLength())))
            let hmacBase64 = hmacData.base64EncodedString(options: .lineLength76Characters)
            return String(hmacBase64)
        }
        return ""
    }
    
    static func randomString(length: Int = 16) -> String {
        var result = [Character](repeating: "-", count: length)
        
        for index in 0 ..< length {
            let range = Int(arc4random_uniform(RandomStringItem.length))
            result[index] = RandomStringItem.characters[range]
        }
        
        return String(result)
    }
}

extension String {
    func encrypt(key: String = RandomStringItem().getEncryptionKey(), ivString: String = RandomStringItem().getEncryptionIv(), options: Int = kCCOptionPKCS7Padding) -> String {
        if let keyData = key.data(using: String.Encoding.utf8),
           let data = self.data(using: String.Encoding.utf8),
           let cryptData    = NSMutableData(length: Int((data.count)) + kCCBlockSizeAES128) {
            
            let keyLength = size_t(kCCKeySizeAES128)
            let operation: CCOperation = UInt32(kCCEncrypt)
            let algoritm: CCAlgorithm = UInt32(kCCAlgorithmAES128)
            let options: CCOptions = UInt32(options)
            
            var numBytesEncrypted: size_t = 0
            let cryptStatus = CCCrypt(operation,
                                      algoritm,
                                      options,
                                      (keyData as NSData).bytes, keyLength,
                                      ivString,
                                      (data as NSData).bytes, data.count,
                                      cryptData.mutableBytes, cryptData.length,
                                      &numBytesEncrypted)
            
            if UInt32(cryptStatus) == UInt32(kCCSuccess) {
                cryptData.length = Int(numBytesEncrypted)
                let base64cryptString = cryptData.base64EncodedString(options: .lineLength64Characters)
                return base64cryptString
            } else {
                return self
            }
        }
        return self
    }
    
    func decrypt(key: String = RandomStringItem().getEncryptionKey(), ivString: String = RandomStringItem().getEncryptionIv(), options: Int = kCCOptionPKCS7Padding) -> String {
        if let keyData = key.data(using: String.Encoding.utf8),
           let data = NSData(base64Encoded: self, options: .ignoreUnknownCharacters),
           let cryptData = NSMutableData(length: Int((data.length)) + kCCBlockSizeAES128) {
            
            let keyLength = size_t(kCCKeySizeAES128)
            let operation: CCOperation = UInt32(kCCDecrypt)
            let algoritm: CCAlgorithm = UInt32(kCCAlgorithmAES128)
            let options: CCOptions = UInt32(options)
            
            var numBytesEncrypted: size_t = 0
            let cryptStatus = CCCrypt(operation,
                                      algoritm,
                                      options,
                                      (keyData as NSData).bytes, keyLength,
                                      ivString,
                                      data.bytes, data.length,
                                      cryptData.mutableBytes, cryptData.length,
                                      &numBytesEncrypted)
            
            if UInt32(cryptStatus) == UInt32(kCCSuccess) {
                cryptData.length = Int(numBytesEncrypted)
                let unencryptedMessage = String(data: cryptData as Data, encoding: String.Encoding.utf8)
                return unencryptedMessage ?? self
            } else {
                return self
            }
        }
        return self
    }
}

extension Data {
    func encrypt(key: String = RandomStringItem().getEncryptionKey(), ivString: String = RandomStringItem().getEncryptionIv(), options: Int = kCCOptionPKCS7Padding) -> Data {
        let data = self as NSData
        if let keyData = key.data(using: String.Encoding.utf8),
           let cryptData = NSMutableData(length: Int((data.count)) + kCCBlockSizeAES128) {
            
            let keyLength = size_t(kCCKeySizeAES128)
            let operation: CCOperation = UInt32(kCCEncrypt)
            let algoritm: CCAlgorithm = UInt32(kCCAlgorithmAES128)
            let options: CCOptions = UInt32(options)
            
            var numBytesEncrypted: size_t = 0
            let cryptStatus = CCCrypt(operation,
                                      algoritm,
                                      options,
                                      (keyData as NSData).bytes, keyLength,
                                      ivString,
                                      (data as NSData).bytes, data.count,
                                      cryptData.mutableBytes, cryptData.length,
                                      &numBytesEncrypted)
            
            if UInt32(cryptStatus) == UInt32(kCCSuccess) {
                cryptData.length = Int(numBytesEncrypted)
                return cryptData as Data
            } else {
                return self
            }
        }
        return self
    }
    
    func decrypt(key: String = RandomStringItem().getEncryptionKey(), ivString: String = RandomStringItem().getEncryptionIv(), options: Int = kCCOptionPKCS7Padding) -> Data {
        let data = self as NSData
        if let keyData = key.data(using: String.Encoding.utf8),
           let cryptData = NSMutableData(length: Int((data.length)) + kCCBlockSizeAES128) {
            
            let keyLength = size_t(kCCKeySizeAES128)
            let operation: CCOperation = UInt32(kCCDecrypt)
            let algoritm: CCAlgorithm = UInt32(kCCAlgorithmAES128)
            let options: CCOptions = UInt32(options)
            
            var numBytesEncrypted: size_t = 0
            let cryptStatus = CCCrypt(operation,
                                      algoritm,
                                      options,
                                      (keyData as NSData).bytes, keyLength,
                                      ivString,
                                      data.bytes, data.length,
                                      cryptData.mutableBytes, cryptData.length,
                                      &numBytesEncrypted)
            
            if UInt32(cryptStatus) == UInt32(kCCSuccess) {
                cryptData.length = Int(numBytesEncrypted)
                return cryptData as Data
            } else {
                return self
            }
        }
        return self
    }
}

extension String: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
    
    func replaceSpaceInURL() -> String{
        let str = self
        let newStr : String = str.replacingOccurrences(of: " ", with: "%20")
        return newStr
    }
}
